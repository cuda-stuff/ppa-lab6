\documentclass{article}
\usepackage{ijcai13}

\usepackage{times}
\usepackage{listings}
\usepackage{color}
\usepackage{url}
\usepackage{textcomp}
\usepackage{eurosym}
\usepackage[table,xcdraw]{xcolor}
\usepackage{graphicx}
\usepackage{ngerman}
\usepackage{inputenc}[utf8]

% proper font encoding in order to print greater than (>) and less than (<) symbols properly
\usepackage[T1]{fontenc}

% Eclipse Java code style listings
% taken from: https://texblog.org/2011/06/11/latex-syntax-highlighting-examples/
\definecolor{javared}{rgb}{0.6,0,0} % for strings
\definecolor{javagreen}{rgb}{0.25,0.5,0.35} % comments
\definecolor{javapurple}{rgb}{0.5,0,0.35} % keywords
\definecolor{javadocblue}{rgb}{0.25,0.35,0.75} % javadoc
\lstdefinestyle{java-eclipse}{language=Java,
	basicstyle=\footnotesize,
	breaklines=true,
	keywordstyle=\color{javapurple}\bfseries,
	stringstyle=\color{javared},
	commentstyle=\color{javagreen},
	morecomment=[s][\color{javadocblue}]{/**}{*/},
	numbers=left,
	numberstyle=\tiny\color{black},
	stepnumber=2,
	numbersep=10pt,
	tabsize=2,
	showspaces=false,
	showstringspaces=false,
	frame=single
}

% Java code style
% taken from: https://www.overleaf.com/latex/examples/syntax-highlighting-in-latex-with-the-listings-package/jxnppmxxvsvk
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstdefinestyle{java-type1}{
	backgroundcolor=\color{white},   % choose the background color
	basicstyle=\footnotesize,        % size of fonts used for the code
	breaklines=true,                 % automatic line breaking only at whitespace
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{mygreen},    % comment style
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	keywordstyle=\color{blue},       % keyword style
	stringstyle=\color{mymauve},     % string literal style
}


\title{Parallel Computing Praktikum - Aufgabenblatt 6}
\author{Dennis Bystrow \\
University of Applied Sciences, Offenburg\\
dbystrow@stud.hs-offenburg.de}

\begin{document}

\maketitle

\begin{abstract}
Dieses Labor spaltet sich in zwei Teile. Zum Einen die Implementierung des parallelen Algorithmus zur Berechnung der Editierdistanz zweier Strings mit CUDA und zum Anderen die Hilfestellung zur Parallelisierung des Weihnachtsgeschäfts des Weihnachtsmannes.
\end{abstract}

\section{Editierdistanz mit CUDA}

Die Editierdistanz berechnet wie viele Editieroperationen notwendig sind, um eine Zeichenkette $A$ mit Länge $n$ in eine andere Zeichenkette $B$ mit Länge $m$ zu überführen. Mögliche Operationen sind das Einfügen, Löschen und Ersetzen eines Zeichens. Ein sequentieller Algorithmus benötigt die Laufzeit $O(n \cdot m)$. Im Folgenden wird ein paralleler Algorithmus mit CUDA implementiert und diskutiert. Implementiert wurde allerdings nur die Variante, die nur einen Thread-Block verwendet.

\subsection{Implementierung mit nur einem Thread-Block}
\label{implementierung}

Wird der Algorithmus so implementiert, dass nur ein Thread-Block verwendet wird, ist die maximale Länge der zu vergleichenden Strings abhängig von der maximalen Anzahl der Threads pro Thread-Block und von der verfügbaren Menge an Device-Memory. Auf dem verwendeten System beträgt die maximale Anzahl Threads pro Block 1'024.

Der Algorithmus erstellt aus den zwei Strings $A$ und $B$ eine Matrix. Einer der Strings darf maximal 1'024 Zeichen lang sein, damit die Matrix maximal 1'024 Reihen besitzt und jede Reihe von einem Thread abgearbeitet werden kann. Die Anzahl der Spalten ist davon abhängig wie viel Speicher dem Device zur Verfügung steht. Zusammengefasst: Die Länge von String $A$ multipliziert mit der Länge von String $B$ muss kleiner als der Device-Memory sein und einer der Strings darf zudem maximal 1'024 Zeichen lang sein.

Würde man mit dieser Implementierung mehr als einen Thread-Block verwenden käme es zu einem Problem, weil man in CUDA nicht zwischen Thread-Blöcken synchronisieren kann und nicht garantiert ist, dass Thread-Blöcke in bestimmter Reihenfolge ausgeführt werden. In dieser Implementierung ist eine untere Reihe der Matrix allerdings vom Ergebnis der vorigen Reihe abhängig, sodass man zu keinem Ergebnis kommen könnte, wenn mehrere Thread-Blöcke verwendet werden würden, die die Strings untereinander naiv aufteilen.

\subsection{Verwendung mehrerer Thread-Blöcke + d}

Wenn man mehrere Thread-Blöcke verwenden möchte, kann man so vorgehen, dass man die zu berechnende Matrix in Kacheln unterteilt und in jeder dieser Kacheln jeweils die Editierdistanz berechnet, indem man den Algorithmus verwendet, der für Aufgabe \ref{implementierung} implementiert wurde.

Allerdings lassen sich nicht alle Kacheln parallel berechnen, weil es zwischen ihnen Abhängigkeiten gibt. Eine Kachel ist abhängig vom Ergebnis des linken Nachbarn, des oberen Nachbarn und des links-oberen Nachbarn. Das bedeutet, dass nur Kacheln, die auf einer Diagonalen liegen, unabhängig voneinander berechnet werden können. Genau genommen werden von den Nachbarkacheln nicht alle Elemente benötigt. Vom linken Nachbarn werden nur die Elemente der letzten Spalte benötigt, vom oberen Nachbarn nur die Elemente der letzten Reihe und vom links-oberen Nachbarn wird nur das letzte Element (unten rechts) benötigt.

Wenn man zwei Strings der Länge $n$ hat und ein Block aus $k$ Threads besteht, dann hat man in der resultierenden Kachelaufteilung $\frac{n}{k} \cdot 2 - 1$ Diagonalen und genau so viele Kernelaufrufe. Die längste Diagonale hat $\frac{n}{k}$ Kacheln und somit können in einem einzelnen Kernelaufruf maximal $\frac{n}{k}$ Kacheln parallel berechnet werden.

\subsection{Rechenbeispiel}

Wenn man davon ausgeht, dass die Matrix-Einträge aus 32-Bit-Integerwerten bestehen, die gesamte Matrix im GPU-Speicher gehalten wird und diese 4GB Global Memory besitzt, dann darf die Länge $n_{max}$ der zu vergleichenden Strings maximal $65'536$ sein.

\begin{equation}
	n_{max} = \frac{2^{32} \cdot 8 \cdot 4}{32} = 65'536
\end{equation}

In dieser Rechnung werden weitere Parameter des Kernel-Aufrufs, die für die Implementierung nötig sein könnten, allerdings nicht beachtet.

Wenn beide Strings die Länge $n = 1'000'000$ haben, dann benötigt man $4 n^2$ Bytes ($3,638$ TB) GPU-Speicher, um die gesamte Matrix im Speicher zu halten.

\subsection{Möglichkeit den Speicherverbrauch zu senken}

Wenn man nicht die gesamte Matrix als Ergebnis benötigt, dann kann man den Speicherverbrauch senken, indem man für einen Kernelaufruf nur diejenigen Teile der bereits berechneten Kacheln im Speicher hält, von denen die Kacheln der aktuellen Diagonalen wirklich abhängen. Konkret sind das die letzte Spalte der jeweiligen linken Nachbarn, die letzte Reihe der jeweiligen oberen Nachbarn und das letzte Element der jeweiligen links-oberen Nachbarn.

\section{Weihnachtsaufgabe}

Der Weihnachtsmann versucht sein Geschäft mit Parallelisierung zu verbessern. Er kann in 20 Stunden mit einem Schlitten 150 Millionen Kinder beliefern. Der parallelisierbare Anteil beträgt hierbei $90\%$.

\subsection{Wer hat Recht?}

Beide Berater haben für sich Recht, da sie unterschiedliche Sichtweisen vertreten. Gustaf \& Son sagt aus, dass mit beliebig vielen Schlitten beliebig viele Kinder beliefert werden könnten. Mit genügend Prozessoren wäre also eine beliebige Problemgröße schaffbar. A.M. Dahl sagt aus, dass er mit beliebig vielen Schlitten, nicht mehr als 10 mal so schnell sein kann, also, dass das Problem mit mehr Schlitten nur bis zu einem bestimmten Punkt schneller berechnet werden kann. Der parallelisierbare Anteil des Problems beträgt $90\%$ und bei diesem Anteil hat Amdahl's Law einen Grenzwert für den Speedup von 10.

\subsection{Wie viel Zeit mit 2 bzw. 3 Zusätzlichen Schlitten nötig wäre}

Nach Gustafson's Law beträgt der Speedup $S(p)$ mit insgesamt 3 bzw. 4 Schlitten jeweils $2.8$ und $3.7$, da der parallelisierbare Anteil bei $90\%$ liegt.

$$S(p) = (1 - 0.9) + p \cdot 0.9$$

Das bedeutet, dass man mit insgesamt 3 Schlitten $8.43$ Stunden und mit insgesamt 4 Schlitten $6.86$ Stunden benötigt.

$$2h + \frac{18h}{2.8} = 8.43h$$

$$2h + \frac{18h}{3.7} = 6.86h$$

\subsection{Wie viele Schlitten nötig wären, um in 4 bzw. 3 Stunden fertig zu werden?}

Ab Schlitten $10$ wird das Problem in unter 4 Stunden gelöst und ab Schlitten $20$ wird das Problem in unter 3 Stunden gelöst.

\subsection{Wie viele Kinder können mit 4 Schlitten in 20 Stunden beliefert werden?}

Der Speedup nach Gustafson mit 4 Schlitten beträgt $S(4) = 3.7$. Es können also $3.7$ mal mehr Kinder in der gleichen Zeit beliefert werden, nämlich $555$ Millionen Kinder.

\subsection{Wie viele Schlitten wären nötig, um in 24 Stunden 2 Mrd. Kinder zu beliefern?}

In diesem Fall liegt der parallelisierbare Anteil bei $91.67\%$. 2 Milliarden Kinder sind $13.33$ mal mehr Kinder als 150 Millionen. Man bräuchte also so viele Schlitten, dass der Speedup bei $13.33$ liegt. Ab Schlitten $15$ wird dieser Speedup erreicht bzw. sogar übertroffen.

\bibliographystyle{named}
\bibliography{labor6}

\end{document}

