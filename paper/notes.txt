1a

Wie lange dürfen die Strings maximal sein?
  So lang wie maximale Anzahl Threads pro Block (z.B. 1024).
  Das beschränkt aber nur den String in der Spalte und nicht den in der Reihe.
  Maximale String Länge in der Reihe ist durch den Memory der Graka beschränkt. Z.B. 1024 * länge String in Reihe <= Memory damit man die Matrix bilden kann.

Problem wenn man mehr als einen Block verwendet?
  Man hat pro Reihe der Matrix einen Thread und diese sind teilweise abhängig voneinander.
  Man hätte also Abhängigkeiten zwischen Blöcken, aber zwischen Blöcken kann man nicht synchronisieren.