#include <stdio.h>
#include <string.h>

__device__ __host__ int rowMajorIndex(int rowLength, int i, int k)
{
	return rowLength * i + k;
}

__device__ __host__ void calcCell(int i, int k, int* editierdistanz_matrix, int rowLength, int rows, char a, char b)
{
	if (a == b)
	{
		// do nothing
		editierdistanz_matrix[rowMajorIndex(rowLength, i, k)] = editierdistanz_matrix[rowMajorIndex(rowLength, i - 1, k - 1)];
	}
	else
	{
		int diag = editierdistanz_matrix[rowMajorIndex(rowLength, i - 1, k - 1)];
		int left = editierdistanz_matrix[rowMajorIndex(rowLength, i, k - 1)];
		int top = editierdistanz_matrix[rowMajorIndex(rowLength, i - 1, k)];

		// use the smallest of the three neighbouring cells (insert, delete, update)
		if (top < left && top < diag)
		{
			editierdistanz_matrix[rowMajorIndex(rowLength, i, k)] = 1 + top;
		}
		else if (left < diag)
		{
			editierdistanz_matrix[rowMajorIndex(rowLength, i, k)] = 1 + left;
		}
		else
		{
			editierdistanz_matrix[rowMajorIndex(rowLength, i, k)] = 1 + diag;
		}
	}
}

__global__ void editierdistanz(char* string_a, char* string_b, int* editierdistanz_matrix, int string_a_length, int string_b_length, int* result)
{
	int myId = blockIdx.x * blockDim.x + threadIdx.x;

	if (myId >= string_a_length)
	{
		return;
	}

	for (int c = 1 - myId; c <= string_b_length + string_a_length - myId; c++)
	{
		if (c >= 1 && c <= string_b_length)
		{
			// myId + 1 and string_a[myId + 1 - 1], string_b[c - 1] because c expects index 0 to not be part of the string but it actually is in this implementation
			calcCell(myId + 1, c, editierdistanz_matrix, string_b_length + 1, string_a_length + 1, string_a[myId + 1 - 1], string_b[c - 1]);
		}
		__syncthreads();
	}
	
	if (myId == 0)
	{
		*result = editierdistanz_matrix[(string_a_length + 1) * (string_b_length + 1) - 1];
	}	
}

void print_matrix(int* matrix, int rowLength, int rows, char* string_a, char* string_b)
{
	printf("    ");
	for (int i = 0; i < rowLength - 1; i++)
	{
		printf("%c ", string_b[i]);
	}
	printf("\n");

	for (int i = 0; i < rows; i++)
	{
		if (i == 0)
		{
			printf("  ");
		}

		for (int k = 0; k < rowLength; k++)
		{
			if (i > 0 && k == 0)
			{
				printf("%c ", string_a[i - 1]);
			}
			printf("%d ", matrix[rowLength * i + k]);
		}
		printf("\n");
	}
}

void initialize_matrix(int* matrix, int rowLength, int rows)
{
	for (int i = 0; i < rows; i++)
	{
		matrix[rowLength * i] = i;
	}

	for (int i = 0; i < rowLength; i++)
	{
		matrix[i] = i;
	}
}

int main(void)
{
	// initializations and host allocations
	char string_a[] = "STAR";
	char* d_string_a;
	char string_b[] = "SAMSTAG";
	char* d_string_b;
	size_t string_a_length = strlen(string_a);
	size_t string_b_length = strlen(string_b);
	int result = 0;
	int* d_result;
	int* editierdistanz_matrix = (int*) calloc((string_a_length + 1) * (string_b_length + 1), sizeof(int));
	int* d_editierdistanz_matrix;
	initialize_matrix(editierdistanz_matrix, string_b_length + 1, string_a_length + 1);
	
	printf("editierdistanz matrix:\n");
	print_matrix(editierdistanz_matrix, string_b_length + 1, string_a_length + 1, string_a, string_b);
	printf("\n");

	// device allocations and copyping to device
	cudaMalloc((void**) &d_string_a, string_a_length);
	cudaMalloc((void**) &d_string_b, string_b_length);
	cudaMalloc((void**) &d_editierdistanz_matrix, (string_a_length + 1) * (string_b_length + 1) * sizeof(int));
	cudaMalloc((void**) &d_result, sizeof(int));
	cudaMemcpy(d_string_a, string_a, string_a_length, cudaMemcpyHostToDevice);
	cudaMemcpy(d_string_b, string_b, string_b_length, cudaMemcpyHostToDevice);
	cudaMemcpy(d_editierdistanz_matrix, editierdistanz_matrix, (string_a_length + 1) * (string_b_length + 1) * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_result, &result, sizeof(int), cudaMemcpyHostToDevice);

	// launch kernel
	const int num_blocks = 1;
	const int num_threads = string_a_length;
	editierdistanz<<<num_blocks, num_threads>>>(d_string_a, d_string_b, d_editierdistanz_matrix, string_a_length, string_b_length, d_result);

	// fetch result
	cudaMemcpy(&result, d_result, sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(editierdistanz_matrix, d_editierdistanz_matrix, (string_a_length + 1) * (string_b_length + 1) * sizeof(int), cudaMemcpyDeviceToHost);

	printf("Results:\n");
	printf("  String A: %s\n", string_a);
	printf("  length: %d\n", string_a_length);
	printf("  String B: %s\n", string_b);
	printf("  length: %d\n", string_b_length);
	printf("  Editierdistanz: %d\n", result);
	printf("\neditierdistanz matrix:\n");
	print_matrix(editierdistanz_matrix, string_b_length + 1, string_a_length + 1, string_a, string_b);
}
